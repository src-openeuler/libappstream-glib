Name:      libappstream-glib
Version:   0.8.2
Release:   1
Summary:   AppStream Abstraction Library

License:   LGPLv2+ and GPLv2+
URL:       http://people.freedesktop.org/~hughsient/appstream-glib/
Source0:   http://people.freedesktop.org/~hughsient/appstream-glib/releases/appstream-glib-%{version}.tar.xz

BuildRequires: glib2-devel >= 2.45.8 docbook-utils gtk-doc gobject-introspection-devel
BuildRequires: libsoup-devel >= 2.51.92 gdk-pixbuf2-devel >= 2.31.5 gtk3-devel gettext
BuildRequires: libuuid-devel libstemmer-devel json-glib-devel >= 1.1.1 meson
BuildRequires: pango-devel rpm-devel sqlite-devel libxslt docbook-style-xsl libcurl
Buildrequires: gperf libarchive-devel fontconfig-devel freetype-devel libcurl-devel libyaml-devel

Requires: gdk-pixbuf2 >= 2.31.5 glib2 >= 2.45.8
Requires: json-glib >= 1.1.1 libsoup >= 2.51.92

Provides: appdata-tools %{name}-builder = %{version}-%{release}
Provides: %{name}-builder-devel = %{version}-%{release}
Obsoletes: appdata-tools < 0.1.9 %{name}-builder < %{version}-%{release}
Obsoletes: %{name}-builder-devel < %{version}-%{release}
Recommends: pngquant

%description
The library provides GObjects and helper methods for easier reading and
Write AppStream metadata. At the same time, it provides a simple DOM 
implementation to achieve more efficient conversion between edit nodes 
and standard XML.

%package devel
Summary: Libraries and header files for developing applications that use appstream-glib
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and header files for developing applications that use appstream-glib

%package help
Summary: Help document for the libappstream-glib package

%description help
Help document for the libappstream-glib package.

%prep
%autosetup -n appstream-glib-%{version} -p1

%build
%meson -Dgtk-doc=true -Dstemmer=true -Ddep11=false
%meson_build

%install
%meson_install

%find_lang appstream-glib

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files -f appstream-glib.lang
%doc AUTHORS  COPYING README.md NEWS
%{_libdir}/{libappstream-glib.so.8*,girepository-1.0/*.typelib}
%{_bindir}/{appstream-util,appstream-compose}
%dir %{_datadir}/bash-completion/completions/
%{_datadir}/bash-completion/completions/appstream-util
%{_mandir}/man1/appstream-util.1.gz
%{_mandir}/man1/appstream-compose.1.gz
%{_bindir}/appstream-builder
%{_libdir}/asb-plugins-5/*.so
%{_datadir}/bash-completion/completions/appstream-builder

%files devel
%{_libdir}/{libappstream-glib.so,pkgconfig/appstream-glib.pc}
%{_includedir}/libappstream-glib/*.h
%{_datadir}/gtk-doc/html/appstream-glib
%{_datadir}/gir-1.0/AppStreamGlib-1.0.gir
%{_datadir}/aclocal/*.m4
%{_datadir}/installed-tests/appstream-glib/*.test
%{_datadir}/gettext/its/{appdata.its,appdata.loc}
%dir %{_includedir}/libappstream-glib

%files help
%doc README.md NEWS
%{_mandir}/man1/{appstream-util.1.gz,appstream-compose.1.gz,appstream-builder.1.gz}

%changelog
* Sat Nov 12 2022 hua <dchang@zhixundn.com> 0.8.2-1
- update to 0.8.2

* Wed Jan 20 2021 Ge Wang <wangge@huawei.com> - 0.7.14-4
- Modify license information

* Tue Nov 26 2019 wutao <wutao61@huawei.com> - 0.7.14-3
- Package init
